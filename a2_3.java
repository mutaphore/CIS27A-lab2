/*
@author: Dewei Chen 
@date: 1-17-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a2_3.java
@description: this program reads the subtotal and the gratuity rate, 
			  then computes the gratuity and total. 
*/

import java.util.Scanner;

public class a2_3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the subtotal and a gratuity rate: ");
		String s1 = input.next();
		String s2 = input.next();
		
		double subtotal = Double.parseDouble(s1);
		double gratuityRate = Double.parseDouble(s2);
		double gratuity = (double)((int)(subtotal * gratuityRate)) / 100;
		double total = gratuity + subtotal;
		
		System.out.print("The gratuity is " + gratuity + " and total is " + total);
		
	}

}
