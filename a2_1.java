/*
@author: Dewei Chen 
@date: 1-17-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a2_1.java
@description: This program determines how to make change for an amount 
			  using only quarters, dimes, nickels, and pennies.
*/

import java.util.Scanner;

public class a2_1 {
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int remainder;
		int numQuarters;
		int numDimes; 
		int numNickels; 
		int numPennies; 
		
		System.out.print("enter number of cents: ");
		int totalCents = input.nextInt();
		
		//Calculate number of each type of coin 
		numQuarters = totalCents / 25;
		remainder = totalCents % 25;
		numDimes = remainder / 10;
		remainder %= 10;
		numNickels = remainder / 5;
		numPennies = remainder % 5;
		
		System.out.println("pennies: " + numPennies);
		System.out.println("nickels: " + numNickels);
		System.out.println("dimes: " + numDimes);
		System.out.println("quarters: " + numQuarters);
				
	}

}
