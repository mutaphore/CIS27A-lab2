/*
@author: Dewei Chen 
@date: 1-17-2012
@class: CIS27A
@instructor: Dave Harden
@filename: a2_2.java
@description: This program converts pounds into kilograms
*/

import java.util.Scanner;

public class a2_2 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a number in pounds: ");
		double pounds = input.nextDouble();
		
		System.out.print(pounds + " pounds is ");
		System.out.print((double)((int)(pounds * 0.454 * 1000)) / 1000 + " kilograms");
		
	}

}
